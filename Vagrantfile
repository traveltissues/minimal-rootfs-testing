# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.require_version ">= 1.7.0"

if ENV["buster"] == "1" then
  DISTRO = "buster"
  DISTRO_VER = "10.3.0"
else
  DISTRO = "stretch"
  DISTRO_VER = "9.12.0"
end
IP = "12"
VMEM = 4096

Vagrant.configure("2") do |config|
  config.vm.define "#{DISTRO}-testing" do |node|
    node.vm.hostname = "#{DISTRO}-testing"
    node.vm.box = "debian/#{DISTRO}64"
    node.vm.box_version = "#{DISTRO_VER}"
    node.ssh.insert_key = false
    node.vm.synced_folder "~/.cache/buildstream", "/home/vagrant/.cache/buildstream"
    node.vm.network :private_network, ip: "192.168.60.#{IP}"
    node.vm.provider "virtualbox" do |vb|
      vb.linked_clone = true
      vb.memory = VMEM
    end

    node.vm.provision :ansible do |a|
      a.verbose = true
      a.playbook = "setup-bst.yml"
      a.inventory_path = "hosts"
      a.extra_vars = {
        ansible_ssh_user: "vagrant",
        ansible_ssh_private_key_file: "~/.vagrant.d/insecure_private_key",
        ansible_host: "192.168.60.#{IP}",
        distro: "#{DISTRO}",
      }
    end
  end
end
